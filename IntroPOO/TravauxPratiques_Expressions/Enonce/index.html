<!DOCTYPE html>
<html lang=fr>
   <head>
      <meta charset="UTF-8">
      <title>Travaux Pratiques</title>
      <link rel="stylesheet" type="text/css" media="all" href="style.css">
   </head>
   <body>
      <h1>Programmation Orientée Objet en Java - Travaux Pratiques - Manipulation d'expressions</h1>
      <p>Sur la base d'un exercice de Dominique Marcadet, CentraleSupélec, 2023</p>
      <h2> Présentation, conception</h2>
      <p>Dans cet exercice nous écrivons un programme permettant de manipuler des expressions arithmétiques simples. Une expression arithmétique simple est composée de nombres, de variables et d'opérateurs binaires (<code>+</code>, <code>-</code>, <code>*</code> et <code>/</code>). Voici quelques exemples d'expressions :</p>
      <div style="background-color: #f1efff;">
         <div class="indent"><span style="color: green;"><code>	2</code></span></div>
         <div class="indent"><span style="color: green;"><code>	x + 5</code></span></div>
         <div class="indent"><span style="color: green;"><code>	x - 7 * y</code></span></div>
         <div class="indent"><span style="color: green;"><code>	(x - 7) * y</code></span></div>
      </div>
      <p>Les seules manipulations envisagées pour l'instant sont l'affichage et l'évaluation.</p>
      <p>Nous n'allons pas représenter ces expressions sous forme de chaînes de caractères dans notre programme ; en effet, l'évaluation d'une telle expression, quand elle est sous forme de chaîne de caractères, nécessite :</p>
      <ul>
         <li>une première étape d'analyse lexicale (reconnaître les différents constituants : nombres, variables, opérateurs, parenthèses…) ;</li>
         <li>une deuxième étape d'analyse syntaxique (déterminer les opérandes des différents opérateurs en tenant compte en particulier des priorités) ; cette étape produit en général ce qu'on appelle un arbre syntaxique (voir ci-dessous) ;</li>
         <li>la dernière étape va alors parcourir l'arbre construit afin de calculer la valeur de l'expression.</li>
      </ul>
      <p>Il existe des outils spécialisés pour réaliser les 2 premières étapes (voir la section <strong>Pour aller plus loin</strong>), nous nous limiterons donc à la définition des classes permettant de représenter une expression sous la forme d'un arbre syntaxique, et nous construirons alors <em>à la main</em> ces arbres.</p>
      <p>La représentation d'une expression sous la forme d'un arbre est assez intuitive : un opérateur a deux opérandes, donc ce sera un nœud interne avec deux fils, et les feuilles de l'arbre seront les nombres et les variables. Ainsi, les expressions ci-dessus seront représentées sous la forme des arbres suivants :</p>
      <img src="expressions1.png" alt="différents arbres">
      <p>Les couleurs identifient les différentes sortes d'éléments présents dans un tel arbre :</p>
      <ul>
         <li>les instances de la classe <span style="color: green;"><code>Nombre</code></span> représenterons les nombres <span style="color: blue;"><strong><span style="font-size: 144%;">2</span></strong></span>, <span style="color: blue;"><strong><span style="font-size: 144%;">5</span></strong></span>, <span style="color: blue;"><strong><span style="font-size: 144%;">7</span></strong></span> ;</li>
         <li>les instances de la classe <span style="color: green;"><code>Variable</code></span> représenterons les variables <span style="color: green;"><strong><span style="font-size: 144%;">x</span></strong></span>, <span style="color: green;"><strong><span style="font-size: 144%;">y</span></strong></span> ;</li>
      </ul>
      <p>Pour les opérateurs, on pourrait choisir de n'avoir qu'une seule classe <span style="color: green;"><code>Operation</code></span> qui mémorise le symbole et effectue l'évaluation en tenant compte de l'opération représentée. Une telle solution serait cependant une <strong>mauvaise solution</strong> car elle s'oppose à la modularité et l'extensibilité (voir le <a target="_blank" class="urllink" href="https://fr.wikipedia.org/wiki/Principe_ouvert/ferm%c3%a9" rel="nofollow">principe ouvert/fermé</a> par exemple). Par ailleurs, la différence entre une addition et une multiplication est bien une différence de comportement, ce qui est un argument majeur pour en faire des classes séparées (une classe définit le comportement de ses instances ; donc si deux objets ont des comportements différents, ils doivent être instances de classes différentes).</p>
      <p>Nous choisissons donc de définir les classes <span style="color: green;"><code>Addition</code></span>, <span style="color: green;"><code>Soustraction</code></span>, <span style="color: green;"><code>Multiplication</code></span> et <span style="color: green;"><code>Division</code></span> pour représenter les opérateurs. Ces classes hériteront de la classe <span style="color: green;"><code>Operation</code></span> qui mémorisera les deux opérandes d'un opérateur. Un opérande est soit un nombre, soit une variable, soit un autre opérateur avec ses deux opérandes… Nous avons besoin de manipuler de manière homogène ces différentes sortes d'éléments, nous introduisons donc la classe <span style="color: green;"><code>Expression</code></span>, super-classe de <span style="color: green;"><code>Nombre</code></span>, <span style="color: green;"><code>Variable</code></span> et <span style="color: green;"><code>Operation</code></span>.</p>
      <p>Le modèle UML coreespondant est donc celui-ci :</p>
      <img src="expressionUML.png" alt="diagramme de classes">
      <p>Comme expliqué ci-dessus, les arbres seront construits manuellement en Java. Le code suivant montre les instructions que nous souhaitons utiliser afin de créer les arbres représentant les expressions utilisées comme exemple :</p>
      <pre class="escaped hlt java hljs language-java">    <span class="hljs-type">Expression</span> <span class="hljs-variable">e1</span> <span class="hljs-operator">=</span> <span class="hljs-keyword">new</span> <span class="hljs-title class_">Nombre</span>( <span class="hljs-number">2</span> );
    <span class="hljs-type">Expression</span> <span class="hljs-variable">e2</span> <span class="hljs-operator">=</span>
        <span class="hljs-keyword">new</span> <span class="hljs-title class_">Addition</span>(
            <span class="hljs-keyword">new</span> <span class="hljs-title class_">Variable</span>( <span class="hljs-string">"x"</span> ),
            <span class="hljs-keyword">new</span> <span class="hljs-title class_">Nombre</span>( <span class="hljs-number">5</span> )
        );
    <span class="hljs-type">Expression</span> <span class="hljs-variable">e3</span> <span class="hljs-operator">=</span>
        <span class="hljs-keyword">new</span> <span class="hljs-title class_">Soustraction</span>(
           <span class="hljs-keyword">new</span> <span class="hljs-title class_">Variable</span>( <span class="hljs-string">"x"</span> ),
           <span class="hljs-keyword">new</span> <span class="hljs-title class_">Multiplication</span>(
               <span class="hljs-keyword">new</span> <span class="hljs-title class_">Nombre</span>( <span class="hljs-number">7</span> ),
               <span class="hljs-keyword">new</span> <span class="hljs-title class_">Variable</span>( <span class="hljs-string">"y"</span> )
           )
        );
    <span class="hljs-type">Expression</span> <span class="hljs-variable">e4</span> <span class="hljs-operator">=</span>
        <span class="hljs-keyword">new</span> <span class="hljs-title class_">Multiplication</span>(
           <span class="hljs-keyword">new</span> <span class="hljs-title class_">Soustraction</span>(
               <span class="hljs-keyword">new</span> <span class="hljs-title class_">Variable</span>( <span class="hljs-string">"x"</span> ),
               <span class="hljs-keyword">new</span> <span class="hljs-title class_">Nombre</span>( <span class="hljs-number">7</span> )
           ),
           <span class="hljs-keyword">new</span> <span class="hljs-title class_">Variable</span>( <span class="hljs-string">"y"</span> )
        );
}
</pre>
      <p>Ce code met en évidence quelques choix de conception qu'il est utile d'expliciter :</p>
      <ul>
         <li>la valeur d'un nombre est donnée comme argument au constructeur ;</li>
         <li>le nom d'une variable est donné comme argument au constructeur ;</li>
         <li>les deux opérandes d'un opérateur sont donnés comme arguments au constructeur.</li>
      </ul>
      <p>Il est important qu'un objet initialisé par un constructeur soit dans un <strong>état consistant</strong>. Si, par exemple, on avait choisi d'avoir un constructeur sans argument pour <span style="color: green;"><code>Variable</code></span> et d'avoir à côté une méthode permettant d'indiquer le nom de cette variable, on aurait à la fois la possibilité d'avoir des variables sans nom (ce qui n'a pas de sens) et des variables qui changent de nom (ce qui n'est pas souhaité ici). Par contre on sait qu'une variable aura une valeur et que cette valeur pourra être modifiée : on peut donc choisir de ne pas préciser de valeur dans le constructeur, et d'utiliser 0 comme valeur initiale : l'instance de <span style="color: green;"><code>Variable</code></span> sera bien dans un état consistant.</p>
      <p>Un raisonnement semblable s'applique aux opérateurs : qu'est-ce qu'une addition qui n'aurait aucun ou un seul opérande ?</p>
      <p>Pour réaliser cet exercice, nous utiliserons une approche itérative en ajoutant les fonctionnalités peu à peu.</p>
      <h2> Première itération : nombres</h2>
      <p>Lancez IntelliJ et créez un projet Java / Maven / GroudId: fr.ensmn.YOURNAME / ArtifactId: <span style="color: green;"><code>expression</code></span>, gardez coché "Add sample code".</p>
      <p>Créez dans le paquetage <span style="color: green;"><code>fr.ensmn.YOURNAME</code></span> une classe <span style="color: green;"><code>Expression</code></span>.<br>
         Cette classe n'aura jamais d'instance directe ⇒ précisez que cette classe est abstraite.<br>
         Nous souhaitons pouvoir afficher et évaluer une expression. L'affichage se fera avec le mécanisme standard de Java (conversion en chaîne de caractères avec la méthode <span style="color: red;"><code>toString()</code></span>), il n'y a donc rien à préciser au niveau de la classe <span style="color: green;"><code>Expression</code></span> pour cela. Pour demander la valeur d'une expression, il est nécessaire d'imposer la signature de la méthode qui devra être utilisée par toutes les sous-classes. Ajoutez dans <span style="color: green;"><code>Expression</code></span> une méthode abstraite et publique <span style="color: red;"><code>getValeur()</code></span> ne prenant pas d'argument et retournant un <span style="color: green;"><code>double</code></span>.
      </p>
      <p>Créez dans le paquetage <span style="color: green;"><code>fr.ensmn.YOURNAME</code></span> une classe <span style="color: green;"><code>Nombre</code></span>, sous-classe d'<span style="color: green;"><code>Expression</code></span>.</p>
      <p>Lorsqu'on indiquer la super-classe <span style="color: green;"><code>Expression</code></span> IntelliJ propose soit de rendre <span style="color: green;"><code>Nombre</code></span> abstraite, soit d'y implémenter les méthodes abstraites héritées de la super-classe <span style="color: green;"><code>Expression</code></span>. Choisissez "Implement methods". IntelliJ ajoute alors automatiquement une définition minimale de la méthode <span style="color: red;"><code>getValeur()</code></span>. IntelliJ ajoute aussi l'annotation <span style="color: gray;"><code>@Override</code></span>, non indispensable mais facilitant la relecture du code.</p>
      <p>Ajoutez dans la classe <span style="color: green;"><code>Nombre</code></span> un attribut privé <span style="color: green;"><code>valeur</code></span> de type <span style="color: green;"><code>double</code></span>. Avec le point d'insertion toujours dans la classe <span style="color: green;"><code>Nombre</code></span>, choisissez <span style="color: blue;">Generate / Constructor</span> dans le menu <span style="color: blue;">Code</span>, vérifiez que <span style="color: green;"><code>valeur</code></span> est sélectionné et cliquez sur <span style="color: blue;">OK</span> : le constructeur est généré automatiquement.</p>
      <p>Corrigez la méthode <span style="color: red;"><code>getValeur()</code></span>.</p>
      <p>Dans la méthode <span style="color: red;"><code>main()</code></span> de la classe classe <span style="color: green;"><code>fr.ensmn.YOURNAME.Main</code></span> introduisez la création d'une instance :</p>
      <pre class="escaped hlt java hljs language-java">    <span class="hljs-type">Nombre</span> <span class="hljs-variable">n</span> <span class="hljs-operator">=</span> <span class="hljs-keyword">new</span> <span class="hljs-title class_">Nombre</span>( <span class="hljs-number">2</span> );
</pre>
      <p>Ajoutez dans <span style="color: red;"><code>main()</code></span> l'instruction :</p>
      <pre class="escaped hlt java hljs language-java">    System.out.println( <span class="hljs-string">"n = "</span> + n );</pre>
      <p>Sauvegardez puis exécutez la classe <span style="color: green;"><code>Main</code></span>. Corrigez la classe <span style="color: green;"><code>Nombre</code></span> pour avoir le comportement souhaité (<code>n = 2.0</code>).<br>
         Note : Une solution simple et classique pour convertir un flottant <span style="color: green;"><code>valeur</code></span> en chaîne de caractères est <span style="color: green;"><code>"" + valeur</code></span>.
      </p>
      <p>Ajoutez dans <span style="color: red;"><code>main()</code></span> l'instruction :</p>
      <pre class="escaped hlt java hljs language-java">        System.out.println( <span class="hljs-string">"n.getValeur() = "</span> + n.getValeur() );</pre>
      <p>et vérifiez son bon fonctionnement.</p>
      <p>Enfin, pour vérifier qu'un nombre peut être manipulé comme une expression, ajoutez dans <span style="color: red;"><code>main()</code></span> les instructions suivantes :</p>
      <pre class="escaped hlt java hljs language-java">        <span class="hljs-type">Expression</span> <span class="hljs-variable">e</span> <span class="hljs-operator">=</span> n;
        System.out.println( <span class="hljs-string">"e = "</span> + e );
        System.out.println( <span class="hljs-string">"e.getValeur() = "</span> + e.getValeur() );
</pre>
      <p>et vérifiez le bon fonctionnement.</p>
      <h2> Deuxième itération : variables</h2>
      <p>En reprennant la démarche utilisée pour <span style="color: green;"><code>Nombre</code></span>, définissez et testez la classe <span style="color: green;"><code>Variable</code></span>. Une variable possède un nom (<span style="color: green;"><code>String</code></span>) initialisé par le constructeur et une valeur (<span style="color: green;"><code>double</code></span>) initialement à <code>0</code>. L'affichage affiche le nom de la variable, et une méthode <span style="color: red;"><code>setValeur()</code></span> permet de modifier sa valeur.</p>
      <h2>Troisième itération : additions</h2>
      <p>Définissez une classe abstraite <span style="color: green;"><code>Operation</code></span>, sous-classe d'<span style="color: green;"><code>Expression</code></span>.</p>
      <p>Cette classe a deux attributs <span style="color: green;"><code>op1</code></span> et <span style="color: green;"><code>op2</code></span>, de type <span style="color: green;"><code>Expression</code></span>, qui sont les opérandes de l'opération : ajoutez-les (ils doivent être <span style="color: green;"><code>protected</code></span> car les sous-classes d'<span style="color: green;"><code>Operation</code></span> auront besoin d'y accéder).</p>
      <p>Générez le constructeur permettant d'initialiser ces deux attributs.</p>
      <p>Définissez la classe <span style="color: green;"><code>Addition</code></span>, sous-classe d'<span style="color: green;"><code>Operation</code></span>.<br>
         <span style="color: green;"><code>Addition</code></span> apparait souligné en rouge : utilisez le <em>quick fix</em> pour ajouter la méthode <span style="color: red;"><code>getValeur()</code></span> et corrigez-la.<br>
         <span style="color: green;"><code>Addition</code></span> reste souligné en rouge : utilisez le <em>quick fix</em> pour ajouter la définition du constructeur qui fait appel au constructeur de la classe <span style="color: green;"><code>Operation</code></span>.
      </p>
      <p>Définissez la méthode d'affichage.</p>
      <p>Dans la méthode <span style="color: red;"><code>main()</code></span> précédente ajoutez une addition et testez son affichage et le calcul de sa valeur.</p>
      <h2>Quatrième itération : autres opérations</h2>
      <p>Procédez de même pour créer les classes <span style="color: green;"><code>Soustraction</code></span>, <span style="color: green;"><code>Multiplication</code></span> et <span style="color: green;"><code>Division</code></span>.</p>
      <p>Les parenthèses sont nécessaires quand une sous-opération utilise un opérateur moins prioritaire ; elles peuvent aussi être présentes quand le critère de priorité ne s'applique pas. À défaut de gérer dans cet exercice cette notion de priorité, on mettra donc systématiquement des parenthèses.</p>
      <p>Ajoutez à la méthode <span style="color: red;"><code>main()</code></span> précédentes des expressions combinant les différents opérateurs et testez les affichages et les calculs de valeur.</p>
      <h2>Pour aller plus loin</h2>
      <h3>Extension des fonctionnalités</h3>
      <p>La représentation des expressions telle que réalisée dans cet exercice est une première étape vers des outils de dérivation formelle, de simplification…</p>
      <h3>Extension du langage</h3>
      <p>Il est assez facile d'enrichir les expressions, par exemple en ajoutant des fonctions (<span style="color: green;"><code>sqrt()</code></span> : calcul de la racine carrée, <span style="color: green;"><code>sin(), cos()...</code></span> : fonctions trigonométriques...).</p>
      <h3>Construction des expressions</h3>
      <p>La réalisation d'un analyseur lexical (reconnaissance des mots du langage) et syntaxique (reconnaissance de la grammaire du langage) peut se faire directement en Java. La difficulté réside ici dans la prise en compte de la priorité des opérateurs (la multiplication et la division sont prioritaires sur l'addition et la soustraction), et le support de l'utilisation des parenthèses. Les algorithmes habituels utilisent une ou plusieurs piles pour gérer ces aspects.<br>
         Il existe cependant des outils spécialisés pour cette tâche, les plus connus actuellement côté Java étant <a class="urllink" href="http://www.antlr.org/" rel="nofollow">ANTLR</a> et <a class="urllink" href="https://www.eclipse.org/Xtext/" rel="nofollow">Xtext</a>.
      </p>
   </body>
</html>

