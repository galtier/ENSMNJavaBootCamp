package org.example;

public class Addition extends Operation {

    public Addition(Expression op1, Expression op2) {
        super(op1, op2);
    }

    @Override
    public double getValeur() {
        return op1.getValeur() + op2.getValeur();
    }

    @Override
    public String toString() {
        return "(" + op1 + " + " + op2 + ")";
    }
}
