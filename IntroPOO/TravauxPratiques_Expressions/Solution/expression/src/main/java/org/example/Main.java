package org.example;

public class Main {
    public static void main(String[] args) {

        Nombre n = new Nombre(2);
        System.out.println("n = " + n);
        System.out.println("n.getValeur() = " + n.getValeur());
        Expression e = n;
        System.out.println("e = " + e);
        System.out.println("e.getValeur() = " + e.getValeur());

        Variable v = new Variable("x");
        System.out.println("v = " + v);
        System.out.println("v.getValeur() = " + v.getValeur());
        v.setValeur(3.5);
        System.out.println("v.getValeur() = " + v.getValeur());
        e = v;
        System.out.println("e = " + e);
        System.out.println("e.getValeur() = " + e.getValeur());

        Addition a = new Addition(n, v);
        System.out.println("a = " + a);
        System.out.println("a.getValeur() = " + a.getValeur());
        e = a;
        System.out.println("e = " + e);
        System.out.println("e.getValeur() = " + e.getValeur());

        Variable y = new Variable("y");
        y.setValeur(2.5);
        Expression e3 =
                new Soustraction(
                        new Variable("x"),
                        new Multiplication(
                                new Nombre(7),
                                y
                        )
                );
        System.out.println("e3 = " + e3);
        System.out.println("e3.getValeur() = " + e3.getValeur());

        Expression e4 =
                new Multiplication(
                        new Nombre(-1),
                        e3
                );
        System.out.println("e4 = " + e4);
        System.out.println("e4.getValeur() = " + e4.getValeur());
    }
}