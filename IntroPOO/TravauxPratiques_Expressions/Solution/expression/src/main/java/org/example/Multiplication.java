package org.example;

public class Multiplication extends Operation {

    public Multiplication(Expression op1, Expression op2) {
        super(op1, op2);
    }

    @Override
    public double getValeur() {
        return op1.getValeur() * op2.getValeur();
    }

    public String toString() {
        return "(" + op1.toString() + " * " + op2.toString() + ")";
    }
}
