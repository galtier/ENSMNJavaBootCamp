package org.example;

public class Nombre extends Expression {

    private double valeur;

    public Nombre(double valeur) {
        this.valeur = valeur;
    }

    @Override
    public double getValeur() {
        return valeur;
    }

    @Override
    public String toString() {
        return "" + valeur;
    }
}
