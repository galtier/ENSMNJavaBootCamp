package org.example;

public abstract class Operation extends Expression {
    protected Expression op1, op2;

    public Operation(Expression op1, Expression op2) {
        this.op1 = op1;
        this.op2 = op2;
    }
}
