package org.example;

public class Variable extends Expression {
    private String nom;
    private double valeur;

    public Variable(String nom) {
        this.nom = nom;
        this.valeur = 0;
    }

    public String toString() {
        return nom;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }
}
