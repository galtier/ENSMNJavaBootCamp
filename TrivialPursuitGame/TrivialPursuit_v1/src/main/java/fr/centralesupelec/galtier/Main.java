package fr.centralesupelec.galtier;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        //System.out.println("Hello " + "world!");
        /*
        String qui = "world";
        System.out.println("Hello " + qui + "!");
        */ /*
        String nom1 = args[0];
        String nom2 = args[1];
        System.out.println("Hello " + nom1 + ", hello " + nom2 + "!");
        */ /*
        Scanner clavier = new Scanner(System.in);
        System.out.println("Comment t'appelles-tu ?");
        String nom = clavier.nextLine();
        System.out.println("Hello " + nom + "!");
        */
        /*
        Scanner clavier = new Scanner(System.in);
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        System.out.print("numéro de la réponse choisie : ");
        String reponse = clavier.nextLine();
        if (reponse.equals("3")) {
            System.out.println("Bonne réponse !");
        } else {
            System.out.println("Mauvaise réponse !");
        }
        */ /*
        Scanner clavier = new Scanner(System.in);
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        System.out.print("numéro de la réponse choisie : ");
        int reponse = clavier.nextInt();
        if (reponse == 3) {
            System.out.println("Bonne réponse !");
        } else {
            System.out.println("Mauvaise réponse !");
        }

        System.out.println("Comment s'appelle la mascotte de Java ?");
        System.out.print("votre réponse : ");
        String nom = clavier.nextLine();
        if (nom.equals("Duke")) {
            System.out.println("Bonne réponse !");
        } else {
            System.out.println("Mauvaise réponse !");
        }
        */
        /*
        Scanner clavier = new Scanner(System.in);
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        System.out.print("numéro de la réponse choisie : ");
        int reponse = clavier.nextInt();
        clavier.skip("\\R");
        if (reponse == 3) {
            System.out.println("Bonne réponse !");
        } else {
            System.out.println("Mauvaise réponse !");
        }

        System.out.println("Comment s'appelle la mascotte de Java ?");
        System.out.print("votre réponse : ");
        String nom = clavier.nextLine();
        if (nom.equals("Duke")) {
            System.out.println("Bonne réponse !");
        } else {
            System.out.println("Mauvaise réponse !");
        }
        */ /*
        Scanner clavier = new Scanner(System.in);
        boolean correct = false;
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        do {
            System.out.print("numéro de la réponse choisie : ");

            int reponse = clavier.nextInt();
            clavier.skip("\\R");
            if (reponse == 3) {
                System.out.println("Bonne réponse !\n");
                correct = true;
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        } while (!correct);

        correct = false;
        System.out.println("Comment s'appelle la mascotte de Java ?");
        while (!correct) {
            System.out.print("votre réponse : ");
            String nom = clavier.nextLine();
            if (nom.equals("Duke")) {
                System.out.println("Bonne réponse !\n");
                correct = true;
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        }
        */
        new Main();
    }

    /*
    Main() {
        Scanner clavier = new Scanner(System.in);
        boolean correct = false;
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        do {
            System.out.print("numéro de la réponse choisie : ");

            int reponse = clavier.nextInt();
            clavier.skip("\\R");
            if (reponse == 3) {
                System.out.println("Bonne réponse !\n");
                correct = true;
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        } while (!correct);

        correct = false;
        System.out.println("Comment s'appelle la mascotte de Java ?");
        while (!correct) {
            System.out.print("votre réponse : ");
            String nom = clavier.nextLine();
            if (nom.equals("Duke")) {
                System.out.println("Bonne réponse !\n");
                correct = true;
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        }
    }
    */ /*
    Main() {
        Scanner clavier = new Scanner(System.in);
        double nbPointsTotal = 0;
        int nbEssais = 0;
        boolean correct = false;
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        do {
            System.out.print("numéro de la réponse choisie : ");

            int reponse = clavier.nextInt();
            nbEssais += 1;
            clavier.skip("\\R");
            if (reponse == 3) {
                System.out.println("Bonne réponse !\n");
                correct = true;
                if (nbEssais == 1) {
                    nbPointsTotal += 1;
                } else if (nbEssais == 2) {
                    nbPointsTotal += 0.5;
                }
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        } while (!correct);

        nbEssais = 0;
        correct = false;
        System.out.println("Comment s'appelle la mascotte de Java ?");
        while (!correct) {
            System.out.print("votre réponse : ");
            nbEssais += 1;
            String nom = clavier.nextLine();
            if (nom.toUpperCase().equals("DUKE")) {
                System.out.println("Bonne réponse !\n");
                correct = true;
                if (nbEssais == 1) {
                    nbPointsTotal += 1;
                } else if (nbEssais == 2) {
                    nbPointsTotal += 0.5;
                }
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        }
        System.out.println("score : " + nbPointsTotal);
    } */

    double nbPointsTotal = 0;
    Main() {
        Scanner clavier = new Scanner(System.in);

        int nbEssais = 0;
        boolean correct = false;
        System.out.println("Java est-il un language...");
        System.out.println("1 : interprété (comme Python)");
        System.out.println("2 : compilé (comme le C)");
        System.out.println("3 : semi-interprété, semi-compilé");
        do {
            System.out.print("numéro de la réponse choisie : ");

            int reponse = clavier.nextInt();
            nbEssais += 1;
            clavier.skip("\\R");
            correct = testeReponse(reponse == 3, nbEssais);
        } while (!correct);

        nbEssais = 0;
        correct = false;
        System.out.println("Comment s'appelle la mascotte de Java ?");
        while (!correct) {
            System.out.print("votre réponse : ");
            nbEssais += 1;
            String nom = clavier.nextLine();
            correct = testeReponse(nom.toUpperCase().equals("DUKE"), nbEssais);
        }
        System.out.println("score : " + nbPointsTotal);
    }

    boolean testeReponse(boolean condition, int n) {
        if (condition) {
            System.out.println("Bonne réponse !\n");
            if (n == 1) {
                nbPointsTotal += 1;
            } else if (n == 2) {
                nbPointsTotal += 0.5;
            }
            return true;
        } else {
            System.out.println("Mauvaise réponse !\n");
            return false;
        }
    }
}