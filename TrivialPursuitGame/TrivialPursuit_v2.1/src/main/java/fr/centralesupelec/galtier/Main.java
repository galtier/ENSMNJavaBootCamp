package fr.centralesupelec.galtier;

import java.util.Collections;
import java.util.Scanner;
import java.util.Vector;

public class Main {
    public static void main(String[] args) {
        new Main();
    }

    Quiz quiz;

    Main() {
        quiz = new Quiz();
        jouer();
    }

    /**
     * Affiche les cartes une à une, attend la réponse de l'utilisateur
     * et indique si elle est correcte ou non avant de passer à la carte suivante.
     * Note : cas d'erreur ignorés (saisie d'une lettre au lieu d'un nombre dans le
     * cas d'une QCM par exemple)
     */
    void jouer() {
        // objet pour lire ce que saisit le joueur
        Scanner clavier = new Scanner(System.in);

        // on parcourt l'ensemble des cartes du quiz
        for (int i = 0; i < quiz.getLesCartes().size(); i++) {
            // on récupère la ième carte
            Carte carte = quiz.getLesCartes().elementAt(i);
            // on affiche la question
            System.out.println(carte.getQuestion());

            // cas 1 : question à choix multiples : la réponse se fait par numéro
            if (carte.getPropositionsIncorrectes().size() != 0) {
                // étape 1 : affichage
                // on mélange les mauvaises et bonne réponse et on préfixe par un entier
                Vector<String> propositions = carte.getPropositionsIncorrectes();
                propositions.add(carte.getReponseCorrecte());
                Collections.shuffle(propositions);
                for (int j = 0; j < propositions.size(); j++) {
                    System.out.println((j + 1) + " : " + propositions.elementAt(j));
                }
                // étape 2 : lecture de la réponse saisie
                System.out.print("numéro de la réponse choisie : ");
                int reponse = clavier.nextInt();
                clavier.skip("\\R");
                // etape 3 : test de la réponse
                // attention les indices dans le vecteur des réponses commence à 0
                // tandis qu'on a numéroté les réponses proposées à partir de 1
                String reponseChoisie = propositions.elementAt(reponse - 1);
                if (carte.isCorrecte(reponseChoisie)) {
                    System.out.println("Bonne réponse !\n");
                } else {
                    System.out.println("Mauvaise réponse !\n");
                }
            }
            // cas 2 : question ouverte
            else {
                System.out.print("votre réponse : ");
                String reponse = clavier.nextLine();

                if (carte.isCorrecte(reponse)) {
                    System.out.println("Bonne réponse !\n");
                } else {
                    System.out.println("Mauvaise réponse !\n");
                }
            }
        }
    }
}