package fr.centralesupelec.galtier;

import java.util.Vector;

/**
 * Cette classe représente un ensemble de cartes.
 *
 * @author Virginie Galtier
 * @see Carte
 */
public class Quiz {

    // liste des cartes / "fiches - questions" composant le quiz
    protected Vector<Carte> lesCartes;

    /**
     * Crée un quiz avec une question à choix multiple et une question ouverte
     */
    public Quiz() {
        // on crée une structure initialement vide pour y ranger les cartes
        lesCartes = new Vector<Carte>();

        String question = "Java est-il un language...";
        String reponseCorrecte = "semi-interprété, semi-compilé";
        Vector<String> propositionsIncorrectes = new Vector<>();
        propositionsIncorrectes.add("interprété (comme Python)");
        propositionsIncorrectes.add("compilé (comme le C)");
        Carte carte = new Carte(question, reponseCorrecte, propositionsIncorrectes);
        lesCartes.add(carte);

        question = "Comment s'appelle la mascotte de Java ?";
        reponseCorrecte = "Duke";
        propositionsIncorrectes = new Vector<>();
        carte = new Carte(question, reponseCorrecte, propositionsIncorrectes);
        lesCartes.add(carte);
    }

    /**
     * Accès aux cartes du quiz
     *
     * @return la liste des cartes constituant le quiz
     */
    public Vector<Carte> getLesCartes() {
        return lesCartes;
    }
}