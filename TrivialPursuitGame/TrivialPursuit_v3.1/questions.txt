question: Java est un langage...
reponseCorrecte: semi-interprété, semi-compilé
propositionsIncorrectes:
interprété (comme Python)
compilé (comme le C)

question: Comment s'appelle la mascotte de Java ?
reponseCorrecte: Duke
propositionsIncorrectes:

question: Quelle entreprise a développé Java ?
reponseCorrecte: Sun Microsystems (maintenant Oracle Corporation)
propositionsIncorrectes:
Apple
Microsoft

question: Quel est le point d'entrée d'un programme Java ?
reponseCorrecte: La méthode main
propositionsIncorrectes:
La méthode start
La méthode run

