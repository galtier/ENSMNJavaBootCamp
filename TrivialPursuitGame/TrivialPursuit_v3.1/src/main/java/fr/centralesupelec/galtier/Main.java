package fr.centralesupelec.galtier;

import java.util.Scanner;

public class Main {
    Quiz quiz;

    Main() {
        quiz = new Quiz("questions.txt");
        jouer();
    }

    public static void main(String[] args) {
        new Main();
    }

    /**
     * Affiche les cartes une à une, attend la réponse de l'utilisateur
     * et indique si elle est correcte ou non avant de passer à la carte suivante.
     * Note : cas d'erreur ignorés (saisie d'une lettre au lieu d'un nombre dans le
     * cas d'une QCM par exemple)
     */
    void jouer() {
        // objet pour lire ce que saisit le joueur
        Scanner clavier = new Scanner(System.in);

        // on parcourt l'ensemble des cartes du quiz
        for (int i = 0; i < quiz.getLesCartes().size(); i++) {
            // on récupère la ième carte
            Carte carte = quiz.getLesCartes().elementAt(i);
            // on affiche la carte
            carte.afficher();
            // on lit la réponse du joueur
            String reponse = clavier.nextLine();
            // on teste si la réponse donnée est correcte
            if (carte.isCorrecte(reponse)) {
                System.out.println("Bonne réponse !\n");
            } else {
                System.out.println("Mauvaise réponse !\n");
            }
        }
    }
}