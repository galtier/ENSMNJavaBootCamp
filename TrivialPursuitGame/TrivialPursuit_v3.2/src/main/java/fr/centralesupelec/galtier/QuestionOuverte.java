package fr.centralesupelec.galtier;

public class QuestionOuverte extends Carte {

    /**
     * Crée une nouvelle carte.
     *
     * @param question        énoncé de la carte (question posée)
     * @param reponseCorrecte réponse attendue
     */
    public QuestionOuverte(String question, String reponseCorrecte) {
        super(question, reponseCorrecte);
    }

    /**
     * Teste si la réponse fournie est celle attendue. Les 2 textes doivent être strictement
     * identiques (longueur, case...).
     *
     * @param reponseFournie texte de la réponse fournie par le joueur
     * @return vrai si la réponse fournie est strictement identique à celle attendue, faux sinon
     */
    public boolean isCorrecte(String reponseFournie) {
        return reponseFournie.equals(reponseCorrecte);
    }

    /**
     * On affiche la question.
     * On invite le joueur à saisir sa réponse.
     */
    public void afficher() {
        System.out.println(question);
        System.out.print("votre réponse : ");
    }
}
