package fr.centralesupelec.galtier;

/**
 * Cette classe représente une "fiche - question" d'un quiz.
 * La question peut être de type QCM ou ouverte.
 *
 * @author Virginie Galtier
 * @see Quiz
 */
public abstract class Carte {
    // question posée par la fiche
    protected String question;

    // réponse attendue
    protected String reponseCorrecte;

    /**
     * Crée une nouvelle carte.
     *
     * @param question                énoncé de la carte (question posée)
     * @param reponseCorrecte         réponse attendue
     */
    public Carte(String question, String reponseCorrecte) {
        this.question = question;
        this.reponseCorrecte = reponseCorrecte;
    }

    /**
     * Teste si la réponse fournie est celle attendue.
     *
     * @param reponseFournie texte de la réponse fournie par le joueur
     * @return vrai si la réponse fournie est celle attendue, faux sinon
     */
    public abstract boolean isCorrecte(String reponseFournie);

    /**
     * Affiche la question et l'invite de réponse appropriée.
     */
    public abstract void afficher();

}
