package fr.centralesupelec.galtier;

import java.util.Collections;
import java.util.Vector;

public class QCM extends Carte {

    // liste des réponses proposées, mélangeant mauvaises et bonne réponses
    private Vector<String> propositions;

    /**
     * Crée une nouvelle carte.
     *
     * @param question                énoncé de la carte (question posée)
     * @param reponseCorrecte         réponse attendue
     * @param propositionsIncorrectes liste des propositions incorrectes
     */
    public QCM(String question, String reponseCorrecte, Vector<String> propositionsIncorrectes) {
        // comme toutes les cartes il y a un énoncé et une réponse correcte, on utilise le constructeur commun :
        super(question, reponseCorrecte);
        // on crée une liste mélangeant les mauvaises réponses et la bonne réponse
        propositions = propositionsIncorrectes;
        propositions.add(reponseCorrecte);
        Collections.shuffle(propositions);
    }

    /**
     * Teste si la réponse fournie est celle attendue.
     * Note: ne traite pas les cas d'erreur
     *
     * @param reponseFournie texte (un numéro) de la réponse fournie par le joueur
     * @return vrai si la réponse fournie est celle attendue, faux sinon
     */
    public boolean isCorrecte(String reponseFournie) {
        // on transforme la chaîne de caractères en entier
        int numeroRepondu = Integer.parseInt(reponseFournie);
        // on cherche quel était le numéro de la bonne réponse dans les propositions mélangées
        int numeroReponseCorrecte = 0;
        for (int i = 0; i < propositions.size(); i++) {
            if (propositions.elementAt(i).equals(reponseCorrecte)) {
                numeroReponseCorrecte = i;
                break;
            }
        }
        // on teste si la réponse fournie est correcte
        return numeroRepondu - 1 == numeroReponseCorrecte;
    }

    /**
     * On affiche un numéro (en partant de 1) devant chaque proposition de réponse.
     * On invite le joueur à saisir un nombre.
     */
    public void afficher() {
        // énoncé
        System.out.println(question);
        // propositions numérotées
        for (int j = 0; j < propositions.size(); j++) {
            System.out.println((j + 1) + " : " + propositions.elementAt(j));
        }
        // invite de réponse
        System.out.print("numéro de la réponse choisie : ");
    }
}
