package fr.centralesupelec.galtier;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Main {
    Quiz quiz;
    int score;

    Main() {
        recupererQuestions("questions.json");
        quiz = new Quiz("questions.json");
        score = 0;
        jouer();
        traiterScore();
    }

    public static void main(String[] args) {
        new Main();
    }

    /**
     * Affiche les cartes une à une, attend la réponse de l'utilisateur
     * et indique si elle est correcte ou non avant de passer à la carte suivante.
     * Si la réponse est correcte, le score est incrémenté.
     * Note : cas d'erreur ignorés (saisie d'une lettre au lieu d'un nombre dans le
     * cas d'une QCM par exemple)
     */
    void jouer() {
        // objet pour lire ce que saisit le joueur
        Scanner clavier = new Scanner(System.in);

        // on parcourt l'ensemble des cartes du quiz
        for (int i = 0; i < quiz.getLesCartes().size(); i++) {
            // on récupère la ième carte
            Carte carte = quiz.getLesCartes().elementAt(i);
            // on affiche la carte
            carte.afficher();
            // on lit la réponse du joueur
            String reponse = clavier.nextLine();
            // on teste si la réponse donnée est correcte
            if (carte.isCorrecte(reponse)) {
                System.out.println("Bonne réponse !\n");
                score++;
            } else {
                System.out.println("Mauvaise réponse !");
                System.out.println("La bonne réponse était : " + carte.getReponseCorrecte() + "\n");
            }
        }
    }


    void traiterScore() {
        String fichierScore = "score.txt";
        File file = new File(fichierScore);

        //cas 1 : le fichier n'existe pas encore
        if (!file.exists()) {
            System.out.println("Vous établissez le premier record, félicitations !");
            enregistrerScore(file);
        }

        // cas 2 : le fichier existe déjà
        else {
            try {
                // on le lit pour voir le score enregistré et son auteur
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String ligne = bufferedReader.readLine();
                // la ligne doit être sous la forme :      Toto : 3
                String recordman = ligne.substring(0, ligne.indexOf(" : "));
                String recordString = ligne.substring(ligne.indexOf(" : ") + 3);
                int record = Integer.parseInt(recordString);

                // on compare le score enregistré au score courant
                if (record > score) {
                    System.out.println("Votre score est " + score + ". C'est en dessous du record établi par " + recordman + " à " + record);
                } else {
                    if (record == score) {
                        System.out.println("Votre score est " + score + ". Félicitations, vous égalez le record établi par " + recordman);
                    } else {
                        System.out.println("Votre score est " + score + ". Félicitations, vous surpassez le record établi par " + recordman + " à " + record);
                    }
                    enregistrerScore(file);
                }
            } catch (FileNotFoundException fnfe) {
                System.err.println("problème dans l'enregistrement du meilleur score");
            } catch (IOException ioe) {
                System.err.println("problème dans l'enregistrement du meilleur score");
            }
        }
    }

    void enregistrerScore(File file) {
        System.out.print("Saisissez votre prénom ou votre pseudo : ");
        Scanner clavier = new Scanner(System.in);
        String id = clavier.nextLine();
        try {
            // objet FileWriter pour écrire dans le fichier
            FileWriter writer = new FileWriter(file);
            writer.write(id + " : " + score);
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void recupererQuestions(String fileName) {
        try {
            // adresse du service permettant de récupérer 5 questions de Trivial Pursuit
            String url = "https://opentdb.com/api.php?amount=5";
            // création d'un objet URL à partir de l'URL spécifiée
            URL apiUrl = new URL(url);
            // ouverture d'une connexion HTTP vers l'URL
            HttpURLConnection connexion = (HttpURLConnection) apiUrl.openConnection();
            // on utilisera une requête HTTP GET
            connexion.setRequestMethod("GET");
            // code de réponse HTTP
            int codeReponseHTTP = connexion.getResponseCode();
            // cas 1 : la réponse HTTP est un succès (code 200)
            if (codeReponseHTTP == HttpURLConnection.HTTP_OK) {
                // création d'un BufferedReader pour lire la réponse
                BufferedReader reader = new BufferedReader(new InputStreamReader(connexion.getInputStream()));
                // création d'un fichier et d'un writer pour écrire la réponse
                File file = new File(fileName);
                FileWriter writer = new FileWriter(file);
                // lecture et écriture ligne par ligne
                String ligne;
                while ((ligne = reader.readLine()) != null) {
                    writer.write(ligne);
                }
                // fermeture des reader et witer
                reader.close();
                writer.close();
            }
            // cas 2 : la requête au service a échoué
            else {
                System.err.println("La requête a échoué avec le code de réponse : " + codeReponseHTTP);
            }
            // Fermeture de la connexion HTTP
            connexion.disconnect();
        } catch (IOException ioe) {
            System.err.println("problème : " + ioe.getMessage());
        }
    }
}
