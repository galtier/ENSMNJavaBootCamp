package fr.centralesupelec.galtier;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

/**
 * Cette classe représente un ensemble de cartes.
 *
 * @author Virginie Galtier
 * @see Carte
 */
public class Quiz {

    // liste des cartes / "fiches - questions" composant le quiz
    protected Vector<Carte> lesCartes;

    /**
     * Crée un quiz en lisant les questions dans un fichier.
     * Le fichier doit avoir l'extension ".txt" et suivre le format maison
     * ou l'extension ".json", respecter la syntaxe json et les champs prévus.
     */
    public Quiz(String fileName) {
        if (fileName.endsWith(".txt")) {
            quizTxt(fileName);
        }
        if (fileName.endsWith(".json")) {
            quizJson(fileName);
        }
    }

    private void quizTxt(String fileName) {
        // on crée une structure initialement vide pour y ranger les cartes
        lesCartes = new Vector<Carte>();

        try {
            // objet FileReader pour lire le fichier
            FileReader fileReader = new FileReader(fileName);
            // objet BufferedReader pour améliorer la lecture
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            // objet pour stocker la ligne lue et éviter une création à chaque tour de boucle
            String ligne;

            // Lecture de chaque ligne du fichier jusqu'à ce qu'il n'y en ait plus.
            while ((ligne = bufferedReader.readLine()) != null) {
                // la ligne est :     question : énoncé de la question
                String question = ligne.substring(10);
                // passage à la ligne suivante
                ligne = bufferedReader.readLine();
                // la ligne est :      reponseCorrecte : texte de la bonne réponse
                String reponseCorrecte = ligne.substring(17);
                // passage à la ligne suivante qui est :     propositionsIncorrectes :
                ligne = bufferedReader.readLine();
                // chaque proposition incorrecte est sur une nouvelle ligne
                // il y a une ligne vide après la dernière proposition incorrecte
                Vector<String> propositionsIncorrectes = new Vector<>();
                while ((ligne = bufferedReader.readLine()).length() != 0) {
                    String propositionIncorrecte = ligne;
                    propositionsIncorrectes.add(propositionIncorrecte);
                }
                // création de la carte
                if (propositionsIncorrectes.size() == 0) {
                    lesCartes.add(new QuestionOuverte(question, reponseCorrecte));
                } else {
                    lesCartes.add(new QCM(question, reponseCorrecte, propositionsIncorrectes));
                }
            }

            // Fermeture du flux de lecture
            bufferedReader.close();
        } catch (FileNotFoundException fnfe) {
            System.err.println("fichier introuvable");
        } catch (IOException ioe) {
            System.err.println("erreur de lecture du fichier : " + ioe.getMessage());
        }
    }

    private void quizJson(String fileName) {
        // on crée une structure initialement vide pour y ranger les cartes
        lesCartes = new Vector<Carte>();

        try {
            // objet Gson permettant de parser un fichier Json
            // cet objet est fourni par une bibliothèque spécifiée dans le fichier pom.xml
            Gson gson = new Gson();
            // lecture de l'arbre du fichier JSON
            JsonObject arbre = gson.fromJson(new FileReader(fileName), JsonObject.class);
            // on récupère le tableau des questions
            JsonArray listesQuestions = arbre.get("results").getAsJsonArray();

            // on parcourt le tableau
            for (JsonElement element : listesQuestions) {
                JsonObject jsonObject = element.getAsJsonObject();
                String questionHtml = jsonObject.get("question").getAsString();
                // au cas où la question contienne du HTML :
                String question = Jsoup.parse(questionHtml).text();
                String reponseCorrecteHtml = jsonObject.get("correct_answer").getAsString();
                String reponseCorrecte = Jsoup.parse(reponseCorrecteHtml).text();
                if (jsonObject.has("incorrect_answers")) {
                    Vector<String> propositionsIncorrectes = new Vector<>();
                    JsonArray propositionsIncorrectesJson = jsonObject.get("incorrect_answers").getAsJsonArray();
                    for (JsonElement proposition : propositionsIncorrectesJson) {
                        String propositionHtml = proposition.getAsString();
                        propositionsIncorrectes.add(Jsoup.parse(propositionHtml).text());
                    }
                    lesCartes.add(new QCM(question, reponseCorrecte, propositionsIncorrectes));
                } else {
                    lesCartes.add(new QuestionOuverte(question, reponseCorrecte));
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("fichier introuvable");
        } catch (IOException e) {
            System.out.println("erreur de lecture du fichier : " + e.getMessage());
        }
    }

    /**
     * Accès aux cartes du quiz
     *
     * @return la liste des cartes constituant le quiz
     */
    public Vector<Carte> getLesCartes() {
        return lesCartes;
    }
}